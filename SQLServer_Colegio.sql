/*
*	BASE DE DATOS COLEGIO 
*	Author: Francisco Cur�n
*/

drop database Colegio;
create database Colegio;
use Colegio;

create table Telefono(
	TelefonoId int identity(1, 1) primary key,
	NumeroTelefono varchar(20) not null unique,
	FechaCreacion datetime not null default(CURRENT_TIMESTAMP),
	FechaModificacion datetime,
	Estatus int not null default(1)
);


create table Usuario(
	UsuarioId int identity(1, 1) primary key,
	NombreUsuario varchar(100) not null unique,
	Clave nvarchar(50) not null,
	TipoUsuario varchar(100) not null,
	FechaCreacion datetime not null default(CURRENT_TIMESTAMP),
	FechaModificacion datetime,
	Estatus int not null default(1)	
);

create table Alumno(
	AlumnoId int identity(1, 1) primary key,	
	UsuarioId int not null foreign key references Usuario(UsuarioId),
	NombreCompleto nvarchar(200) not null,
	ApellidoCompleto nvarchar(200) not null,
	FechaNacimiento date not null,
	Direccion nvarchar(200) not null,
	TelefonoId int foreign key references Telefono(TelefonoId),
	FechaCreacion datetime not null default(CURRENT_TIMESTAMP),
	FechaModificacion datetime,
	Estatus int not null default(1)	
);

create table Profesor(
	ProfesorId int identity(1, 1) primary key,		
	UsuarioId int not null foreign key references Usuario(UsuarioId),
	NombreCompleto nvarchar(200) not null,
	ApellidoCompleto nvarchar(200) not null,
	FechaNacimiento date not null,
	Direccion nvarchar(200) not null,
	TelefonoId int foreign key references Telefono(TelefonoId),
	FechaCreacion datetime not null default(CURRENT_TIMESTAMP),
	FechaModificacion datetime,
	Estatus int not null default(1)	
);

create table Curso(
	CursoId int identity(1, 1) primary key,
	Nombre varchar(50) not null unique,
	FechaCreacion datetime not null default(CURRENT_TIMESTAMP),
	FechaModificacion datetime,
	Estatus int not null default(1)	
);


create table CursoProfesor(
	CursoProfesorId int identity(1, 1) primary key,
	ProfesorId int not null foreign key references Profesor(ProfesorId),
	CursoId int not null foreign key references Curso(CursoId),
	FechaCreacion datetime not null default(CURRENT_TIMESTAMP),
	FechaModificacion datetime,
	Estatus int not null default(1)	
);

create table CursoAlumno(
	CursoAlumnoId int identity(1, 1) primary key,
	AlumnoId int not null foreign key references Alumno(AlumnoId),
	CursoId int not null foreign key references Curso(CursoId),
	FechaCreacion datetime not null default(CURRENT_TIMESTAMP),
	FechaModificacion datetime,
	Estatus int not null default(1)	
);

-- Borrar
/*
drop table CursoProfesor;
drop table CursoAlumno;
drop table Alumno;
drop table Profesor;
drop table Usuario;
drop table Telefono;
drop table Curso;*/



-- Datos
insert into Telefono(NumeroTelefono) values ('1111-1111');
insert into Telefono(NumeroTelefono) values ('2222-2222');
insert into Telefono(NumeroTelefono) values ('3333-3333');
insert into Telefono(NumeroTelefono) values ('4444-4444');
insert into Telefono(NumeroTelefono) values ('5555-5555');
insert into Telefono(NumeroTelefono) values ('6666-6666');
insert into Telefono(NumeroTelefono) values ('7777-7777');
insert into Telefono(NumeroTelefono) values ('8888-8888');
insert into Telefono(NumeroTelefono) values ('9999-9999');
-- select * from Telefono;

insert into Usuario(NombreUsuario, Clave, TipoUsuario) values ('josejfsol@gmail.com', HASHBYTES('SHA1', 'josejfsol'), 'Estudiante');
insert into Usuario(NombreUsuario, Clave, TipoUsuario) values ('mariaermarr@gmail.com', HASHBYTES('SHA1', 'mariaermarr'), 'Estudiante');
insert into Usuario(NombreUsuario, Clave, TipoUsuario) values ('vilmaecper@gmail.com', HASHBYTES('SHA1', 'vilmaecper'), 'Estudiante');
insert into Usuario(NombreUsuario, Clave, TipoUsuario) values ('josepcper@gmail.com', HASHBYTES('SHA1', 'josepcper'), 'Estudiante');
insert into Usuario(NombreUsuario, Clave, TipoUsuario) values ('marcoseoort@gmail.com', HASHBYTES('SHA1', 'marcoseoort'), 'Estudiante');

insert into Usuario(NombreUsuario, Clave, TipoUsuario) values ('carloseppal@gmail.com', HASHBYTES('SHA1', 'carlosseppal'), 'Profesor');
insert into Usuario(NombreUsuario, Clave, TipoUsuario) values ('jorgemrpai@gmail.com', HASHBYTES('SHA1', 'jorgemrpai'), 'Profesor');
insert into Usuario(NombreUsuario, Clave, TipoUsuario) values ('joselrpai@gmail.com', HASHBYTES('SHA1', 'joselrpai'), 'Profesor');
insert into Usuario(NombreUsuario, Clave, TipoUsuario) values ('marcojmsag@gmail.com', HASHBYTES('SHA1', 'marcojmsag'), 'Profesor');
insert into Usuario(NombreUsuario, Clave, TipoUsuario) values ('sucelyavpaz@gmail.com', HASHBYTES('SHA1', 'sucelyavpaz'), 'Profesor');
-- select * from Usuario;


insert into Alumno(UsuarioId, NombreCompleto, ApellidoCompleto, FechaNacimiento, Direccion, TelefonoId) values (1, 'Jos� Jos�', 'Franco Solis', '27/12/2009', 'Barrio el centro', 1);
insert into Alumno(UsuarioId, NombreCompleto, ApellidoCompleto, FechaNacimiento, Direccion, TelefonoId) values (2, 'Maria Enriqueta', 'Rosales Marroquin', '04/01/2009', '4 de mayo', 2);
insert into Alumno(UsuarioId, NombreCompleto, ApellidoCompleto, FechaNacimiento, Direccion, TelefonoId) values (3, 'Vilma Eugenia', 'Castro Perez', '03/01/2017', 'Colonia la palma', 3);
insert into Alumno(UsuarioId, NombreCompleto, ApellidoCompleto, FechaNacimiento, Direccion, TelefonoId) values (4, 'Jos� Pablo', 'Castro Perez', '03/08/2009', 'Colonia la palma', 3);
insert into Alumno(UsuarioId, NombreCompleto, ApellidoCompleto, FechaNacimiento, Direccion, TelefonoId) values (5, 'Marcos Efrain', 'Orellana Ortiz', '11/08/2014', 'Barrio verde', 4);
-- select * from Alumno;

insert into Profesor(UsuarioId, NombreCompleto, ApellidoCompleto, FechaNacimiento, Direccion, TelefonoId) values (6, 'Carlos Ernesto', 'Pineda Palma', '05/03/1983', 'Guatemala', 5);
insert into Profesor(UsuarioId, NombreCompleto, ApellidoCompleto, FechaNacimiento, Direccion, TelefonoId) values (7, 'Jorge Mario', 'Rosas Paiz', '28/11/1968', 'Guatemala', 6);
insert into Profesor(UsuarioId, NombreCompleto, ApellidoCompleto, FechaNacimiento, Direccion, TelefonoId) values (8, 'Jos� Luis', 'Pe�a Lima', '19/01/1974', 'Zacapa', 7);
insert into Profesor(UsuarioId, NombreCompleto, ApellidoCompleto, FechaNacimiento, Direccion, TelefonoId) values (9, 'Marco Josue', 'Molina Sagastume', '28/11/1967', 'Zacapa', 8);
insert into Profesor(UsuarioId, NombreCompleto, ApellidoCompleto, FechaNacimiento, Direccion, TelefonoId) values (10, 'Sucely Alejandra', 'Valladares Paz', '29/10/1960', 'Chiquimula', 9);
-- select * from Profesor;

insert into Curso(Nombre) values ('Matem�tica I');
insert into Curso(Nombre) values ('Lengua');
insert into Curso(Nombre) values ('Ciencias Sociales');
insert into Curso(Nombre) values ('Ciencias Naturales');
insert into Curso(Nombre) values ('Educacion F�sica');
insert into Curso(Nombre) values ('Pl�stica');
insert into Curso(Nombre) values ('Computaci�n I');
insert into Curso(Nombre) values ('M�sica');
insert into Curso(Nombre) values ('�tica');
insert into Curso(Nombre) values ('Ingles I');
-- select * from Curso;

insert into CursoProfesor(ProfesorId, CursoId) values (1, 1);
insert into CursoProfesor(ProfesorId, CursoId) values (1, 2);
insert into CursoProfesor(ProfesorId, CursoId) values (2, 3);
insert into CursoProfesor(ProfesorId, CursoId) values (2, 4);
insert into CursoProfesor(ProfesorId, CursoId) values (3, 5);
insert into CursoProfesor(ProfesorId, CursoId) values (3, 6);
insert into CursoProfesor(ProfesorId, CursoId) values (4, 7);
insert into CursoProfesor(ProfesorId, CursoId) values (4, 8);
insert into CursoProfesor(ProfesorId, CursoId) values (5, 9);
insert into CursoProfesor(ProfesorId, CursoId) values (5, 10);
-- select * from CursoProfesor;

insert into CursoAlumno(AlumnoId, CursoId) values (1, 1);
insert into CursoAlumno(AlumnoId, CursoId) values (1, 2);
insert into CursoAlumno(AlumnoId, CursoId) values (2, 3);
insert into CursoAlumno(AlumnoId, CursoId) values (2, 4);
insert into CursoAlumno(AlumnoId, CursoId) values (3, 5);
insert into CursoAlumno(AlumnoId, CursoId) values (3, 6);
insert into CursoAlumno(AlumnoId, CursoId) values (4, 7);
insert into CursoAlumno(AlumnoId, CursoId) values (4, 8);
insert into CursoAlumno(AlumnoId, CursoId) values (5, 9);
insert into CursoAlumno(AlumnoId, CursoId) values (5, 10);
-- select * from CursoAlumno;
-- select A.NombreCompleto, C.Nombre from CursoAlumno as CA left join Alumno as A on CA.AlumnoId = A.AlumnoId left join Curso as C on CA.CursoId = C.CursoId;